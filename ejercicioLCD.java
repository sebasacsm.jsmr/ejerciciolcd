import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class ejercicioLCD {

// Variable estatica para indicar el String que termina el ciclo
    static final String TERMINAR = "0,0";
    
    public static void main(String[] args) {

// Array para almacenar la N cantidad de Entradas ingresadas por el usuario
    List<String> arrayDatos = new ArrayList<>();

        try {

            try (Scanner entrada = new Scanner(System.in)) {
                    String dato;

                    // se crea ciclo do - while para agregar datos al arrayDatos hasta que el usuario ingrese 0,0
                do
                {
                    System.out.print("Entrada: ");
                    dato = entrada.next();
                    // se comprar el String dato capturado del teclado con la variable TERMINAR (0,0), si es diferente a 0,0 se almacena
                    if(!dato.equalsIgnoreCase(TERMINAR)) {
                    arrayDatos.add(dato);
                    }

                }while (!dato.equalsIgnoreCase(TERMINAR));

                System.out.println("\n Salida: "+ arrayDatos);
            }
        }catch (Exception ex) 
        {
            System.out.println("Error: "+ex.getMessage());
        }
    }
}